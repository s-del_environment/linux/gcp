# GCP を利用した Linux 環境の構築手順
[Google Cloud Platform](https://ja.wikipedia.org/wiki/Google_Cloud_Platform) の Compute Engine にて [Linux](https://ja.wikipedia.org/wiki/Linux) の [VM](https://ja.wikipedia.org/wiki/%E4%BB%AE%E6%83%B3%E6%A9%9F%E6%A2%B0) 環境を構築し、[RLogin](https://ja.wikipedia.org/wiki/RLogin) から [SSH](https://ja.wikipedia.org/wiki/Secure_Shell) 接続を行う手順の備忘録。  


## 利用環境
- Windows 10 Home 1909 (64bit)
- RLogin (x64) Version 2.25.5 (2020/09/25)
    - [公式サイト](http://nanno.dip.jp/softlib/man/rlogin/)
    - [リポジトリ (github.com)](https://github.com/kmiya-culti/RLogin)


## 環境構築手順
1. [プロジェクトと VM インスタンス作成](./プロジェクトとVMインスタンス作成.md)
1. [SSH によるリモート接続](./SSHによるリモート接続.md)


## 参考
[これから始めるGCP（GCE）　安全に無料枠を使い倒せ - Qiita](https://qiita.com/Brutus/items/22dfd31a681b67837a74)